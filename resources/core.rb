actions :add, :remove

attribute :name, :kind_of => String, :name_attribute => true
attribute :language, :kind_of => String, :default => 'english'
attribute :config_directory, :kind_of => String, :default => 'ext_solr_9_0_0'

def initialize(*args)
	super
	@action = :add
end