actions :add

# https://docs.typo3.org/typo3cms/extensions/solr/Appendix/VersionMatrix.html
attribute :solr, :kind_of => String, :default => '7.6.0'
attribute :tika_server, :kind_of => String, :default => '1.23'
attribute :extension, :kind_of => String, :default => '9.0.0'

def initialize(*args)
	super
	@action = :add
end