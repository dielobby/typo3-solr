name 'typo3_solr'
maintainer 'Paul Ilea'
maintainer_email 'p.ilea@die-lobby.de'
license 'All rights reserved'
description 'Installs/Configures TYPO3 EXT:solr'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '2.0.0'
recipe 'typo3_solr::default', 'Main recipe'
issues_url 'https://bitbucket.org/dielobby/typo3-solr/issues'
source_url 'https://bitbucket.org/dielobby/typo3-solr'

depends 'apt'
