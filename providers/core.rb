use_inline_resources

action :add do
	new_resource.updated_by_last_action(false)
	# execute "#{node['typo3_solr']['solr']['solr_home']}/bin/solr create -c #{new_resource.name}"
	create_url = "http://localhost:8983/solr/admin/cores?action=CREATE&name=#{new_resource.name}&configSet=#{new_resource.config_directory}&schema=#{new_resource.language}/schema.xml&dataDir=../../data/#{new_resource.name}"
	puts create_url
	Net::HTTP.get_response(URI.parse(create_url))
	new_resource.updated_by_last_action(true)
end

action :remove do
	execute "#{node['typo3_solr']['solr']['solr_home']}/bin/solr delete -c #{new_resource.name}"
	new_resource.updated_by_last_action(true)
end