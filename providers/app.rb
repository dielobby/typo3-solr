use_inline_resources

action :add do
	# Install requirements
	package 'lsof'

	# Create the solr user
	user 'solr' do
		comment 'Solr User'
		home node['typo3_solr']['solr']['solr_home']
		shell '/bin/bash'
		# openssl passwd -1 "mypassword"
		password '$1$YwUKq1QX$qIVeFlybWqOKJjRLed29j'
	end

	# Fetch and unpack the wanted solr release from our deploy server
	# TODO Add a fallback to an official server if the release is not available on our server
	bash 'Fetch Solr' do
		user 'vagrant'
		timeout 3000000
		action :run
		only_if {
			not ::File.exists?("#{Chef::Config['file_cache_path']}/apache-solr-#{new_resource.solr}.tar.gz")
		}

		code <<-EOF
			wget -P "#{Chef::Config['file_cache_path']}/" https://bitbucket.org/dielobby/typo3-solr/downloads/solr-#{new_resource.solr}.tgz;
			mv "#{Chef::Config['file_cache_path']}/solr-#{new_resource.solr}.tgz" "#{Chef::Config['file_cache_path']}/apache-solr-#{new_resource.solr}.tar.gz";
		EOF
	end

	# Move solr and adjust permissions to the final destination
	bash 'Move Solr' do
		user 'root'
		timeout 3000000
		action :run
		only_if {
			not ::File.exists?("#{node['typo3_solr']['solr']['solr_home']}/README.txt")
		}

		code <<-EOF
			cd #{Chef::Config['file_cache_path']};
			tar -xzf apache-solr-#{new_resource.solr}.tar.gz;
			mv solr-#{new_resource.solr} #{node['typo3_solr']['solr']['solr_home']};
			chown solr:root -R #{node['typo3_solr']['solr']['solr_home']};
		EOF
	end

	# Add systemd starter script
	template "#{node['typo3_solr']['solr']['systemd']}/solr.service" do
		source 'solr.service.erb'
		variables(:app => new_resource.solr)
		mode 0644
		cookbook 'typo3_solr'
	end
	execute 'systemctl enable solr'

	# Fetch the solr TYPO3 extension
	bash 'Fetch TYPO3 Solr' do
		user 'vagrant'
		timeout 3000000
		action :run
		only_if {
			not ::File.exists?("#{Chef::Config['file_cache_path']}/ext-solr-#{new_resource.extension}.tgz")
		}

		code <<-EOF
			wget -P "#{Chef::Config['file_cache_path']}/" https://bitbucket.org/dielobby/typo3-solr/downloads/ext-solr-#{new_resource.extension}.tgz;
			cd #{Chef::Config['file_cache_path']};
			tar -xzf ext-solr-#{new_resource.extension}.tgz;
		EOF
	end

	# Copy extension configuration to the final location
	full_extension_version = new_resource.extension
	if full_extension_version.count('.') == 1
		full_extension_version = full_extension_version + '.0'
	end
	bash 'Copy TYPO3 Solr configuration' do
		user 'root'
		timeout 3000000
		action :run

		code <<-EOF
			cd #{node['typo3_solr']['solr']['solr_home']};
			rm -rf server/solr/configsets/ && cp -a #{Chef::Config['file_cache_path']}/ext-solr-#{full_extension_version}/Resources/Private/Solr/configsets server/solr/configsets/;
			cp -a #{Chef::Config['file_cache_path']}/ext-solr-#{full_extension_version}/Resources/Private/Solr/solr.xml server/solr/;
			chown solr:root -R #{node['typo3_solr']['solr']['solr_home']};
		EOF
	end

	# start solr and wait some time (otherwise the "add core" calls are maybe not executed, because the server is not started)
	execute 'systemctl restart solr'
	execute 'sleep 5'

	# Setup Tika server
	directory node['typo3_solr']['solr']['tika_home'] do
		owner 'root'
		group 'root'
		mode 0770
		action :create
	end

	bash "#{node['typo3_solr']['solr']['tika_home']}/tika-server-#{new_resource.tika_server}.jar" do
		user 'vagrant'
		timeout 3000000
		action :run
		only_if {
			not ::File.exists?("#{node['typo3_solr']['solr']['tika_home']}/tika-server-#{new_resource.tika_server}.jar")
		}

		code <<-EOF
			wget -P "#{node['typo3_solr']['solr']['tika_home']}/" https://bitbucket.org/dielobby/typo3-solr/downloads/tika-server-#{new_resource.tika_server}.jar;
		EOF
	end

	template "#{node['typo3_solr']['solr']['systemd']}/tika-server.service" do
		source 'tika-server.service.erb'
		variables(:app => new_resource.tika_server)
		mode 0644
		cookbook 'typo3_solr'
	end
	execute 'systemctl enable tika-server'
	execute 'systemctl start tika-server'

	# done
	new_resource.updated_by_last_action(true)
end
