default['java']['jdk_version'] = '8'

default['typo3_solr']['solr']['solr_home'] = '/opt/solr'
default['typo3_solr']['solr']['tika_home'] = '/opt/tika'
default['typo3_solr']['solr']['systemd'] = '/lib/systemd/system'