Description
===========

This chef cookbook is based on the shell-scripts which are distributed with the EXT:solr (thank you Ingo Renner!). The
cookbook offers two LWRPs which should be enough to setup a solr app with unlimited solr cores.

Since version 2.0 the cookbook works only with the TYPO3 Solr 7+. 

Requirements
============

Platform:

* Debian, Ubuntu

The following Opscode cookbooks are dependencies:

Attributes
==========

* `node[:typo3_solr][:solr][:solr_home]` - Path to the directory in which the apps should be located

Usage
=====

The following LWRPs are included:

`typo3_solr_app` is used to create a new webapp with the given configuration

*For instances requiring Apache Solr < 4*

```ruby
typo3_solr_app "MySolrAppName" do
  solr '3.5.0'
  extension '2.2'
  plugin '1.2.0'
  languages %w{ german english french italian generic hungarian }
end
```

*For instances requiring Apache Solr > 4*

```ruby
typo3_solr_app "MySolrAppName" do
  solr '4.7.1'
  extension '3.0'
  plugin_access '2.0'
  plugin_utils '1.1'
  plugin_lang '3.1'
  languages %w{ german english french italian generic hungarian }
end
```

`typo3_solr_core` is used to attach cores to a already created webapp

```ruby
typo3_solr_core "de-TestWeb-123-de_DE" do
  language 'german'
  app 'MySolrAppName'
  action :add
end
```

Integration Example
===================

``` ruby
node.default['typo3_site']['hostname'] = false
base_hostname = node['typo3_site']['hostname'] || node['ubuntu_base']['hostname']
if node['typo3_site']['solr']['init']
	include_recipe 'java'
	include_recipe 'typo3_solr'

	typo3_solr_app 'solr' do
		solr node['typo3_site']['solr']['version_solr']
		extension node['typo3_site']['solr']['version_extension']
		tika_server node['typo3_site']['solr']['version_tika_server']
		plugin_access node['typo3_site']['solr']['version_plugin_access']
		plugin_utils node['typo3_site']['solr']['version_plugin_utils']
		plugin_lang node['typo3_site']['solr']['version_plugin_lang']
		languages node['typo3_site']['solr']['languages']
	end

	node['typo3_site']['solr']['cores'].each do |core_data|
		typo3_solr_core core_data['name'] do
			language core_data['language']
			app 'solr'
			action :add
		end
	end

	execute 'solr-updateConnections' do
		command "php #{node['typo3_site']['webroot']}/#{node['typo3_site']['hostname']}/#{node['typo3_site']['web_directory']}/typo3/cli_dispatch.phpsh solr updateConnections || true"
		user 'vagrant'
		group data_bag['groupId']
		action :run
	end
end
```
